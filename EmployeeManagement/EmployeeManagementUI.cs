﻿public class EmployeeManagementUI
{
    public EmployeeManagement employeeManagement { get; set; }

    public EmployeeManagementUI()
    {
        // đối tượng này dùng để quản lý và lưu trữ nhân viên
        employeeManagement = new EmployeeManagement();
    }

    public void PrintEmployees()
    {

        Employee[] employees = employeeManagement.GetEmployees();
        // TODO: Hiển thị thông tin của nhân viên
        // Định dạng in ra: Tên: [name], Tuổi: [age], Chức vụ: [position], Lương: [salary]
        Console.WriteLine("Hiển thị thông tin của nhân viên");
    }

    public void AddEmployee()
    {
        // TODO: Yêu cầu người dùng nhập tên, tuổi, chức vụ và lương của nhân viên mới
        Console.WriteLine("Tạo một đối tượng Employee và thêm vào danh sách");
        // Tạo một đối tượng Employee và gọi hàm employeeManagement.AddEmployee();
    }

    public void RemoveEmployee()
    {
        // TODO: Yêu cầu người dùng nhập tên của nhân viên cần xóa
        // gọi hàm employeeManagement.RemoveEmployee("hehehee");
        Console.WriteLine("Tìm và loại bỏ nhân viên có tên trùng khớp từ danh sách");
    }
}
